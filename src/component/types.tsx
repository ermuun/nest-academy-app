export type PaddingType = {
    top?: number;
    bottom?: number;
    left?: number;
    right?: number;
    size?: Array<number>;
};