import styled from 'styled-components/native';
import _ from 'lodash';
import { PaddingType } from '../types';

const baseSpace = 4;

export const Padding = styled.View<PaddingType>`
    padding-top: ${props => _.isArray(props.size)
        ? (props.size[0] * baseSpace || 0) + "px"
        : (props.top * baseSpace || 0) + "px"};
    padding-right: ${props => _.isArray(props.size)
        ? (props.size[1] * baseSpace || 0) + "px"
        : (props.right * baseSpace || 0) + "px"};
    padding-bottom: ${props => _.isArray(props.size)
        ? (props.size[2] * baseSpace || 0) + "px"
        : (props.bottom * baseSpace || 0) + "px"};
    padding-left: ${props => _.isArray(props.size)
        ? (props.size[3] * baseSpace || 0) + "px"
        : (props.left * baseSpace || 0) + "px"};
`