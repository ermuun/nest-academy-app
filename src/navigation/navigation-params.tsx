export enum NavigationRoutes {
    Home = 'Home',
    Test = 'Test'
}
export interface NavigationPayload<T> {
    props: T;
}
export type NavigatorParamList = {
    [NavigationRoutes.Home]: NavigationPayload<any>;
    [NavigationRoutes.Test]: NavigationPayload<any>;
};
