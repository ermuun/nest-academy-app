import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { HomeScreen, TestScreen } from '../screen';
import { NavigationRoutes, NavigatorParamList } from './navigation-params';

const RootStack = createStackNavigator<NavigatorParamList>();

export const RootNavigationContainer = () => {
    return (
        <NavigationContainer>
            <RootStack.Navigator>
                <RootStack.Screen
                    name={NavigationRoutes.Home}
                    component={HomeScreen}
                    options={{ headerShown: false, gestureEnabled: true }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.Test}
                    component={TestScreen}
                    options={{ headerShown: false, gestureEnabled: true }}
                />
            </RootStack.Navigator>
        </NavigationContainer>
    );
};