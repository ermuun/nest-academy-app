import React from 'react';
import { SafeAreaView, Text } from 'react-native';
import { Padding } from '../../component';

export const HomeScreen = () => {
    return (
        <SafeAreaView style={{ backgroundColor: '#FFFFFF' }}>
            <Padding size={[5, 10, 5, 10]}>
                <Text>Home screens</Text>
            </Padding>
        </SafeAreaView>
    );
};
