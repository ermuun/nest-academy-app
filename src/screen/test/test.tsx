import React from 'react';
import { SafeAreaView, Text } from 'react-native';

export const TestScreen = () => {
    return (
        <SafeAreaView style={{ backgroundColor: '#FFFFFF' }}>
            <Text>Design Components</Text>
        </SafeAreaView>
    );
};
